package com.example.telegrambotcode.message;

import com.example.telegrambotcode.sevice.ActionService;
import com.example.telegrambotcode.sevice.redis.RedisHelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
@Service
public class MessageBot {
    @Autowired
    RedisHelperService redisHelperService;
    @Autowired
    ActionService actionService;

    public String messageStartAndStop( Update update){
        if(redisHelperService.getByChatId(update.getMessage().getChatId())!=null
                && redisHelperService.getByChatId(update.getMessage().getChatId()).isActive()
                && update.getMessage().getText()!=null && update.getMessage().getText().equals("/start")  ) {
            return "\n" +
                    "Sənin aktiv olan sorğun var. Sorğuya davam edə və ya \"/stop\" əmrini yazmaqla bu sorğunu dayandıra bilərsən";

        }
        if( update.getMessage().getText()!=null && update.getMessage().getText().equals("/stop") ){
            redisHelperService.updateIsActive(redisHelperService.getByChatId(update.getMessage().getChatId()),update,false);
            return "Aktiv sorğu dayandırıldı! Yeni sorğu yaratmaq üçün \"/start\" yaz və ya \"/start\" yazısına kliklə";
        }
        return null;
    }

}
