package com.example.telegrambotcode.Message;

import com.example.telegrambotcode.sevice.util.RedisHelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
@Service
public class MessageBot {
    @Autowired
    RedisHelperService redisHelperService;
    public String message( Update update){
        if(redisHelperService.getByChatId(update.getMessage().getChatId())!=null
                && redisHelperService.getByChatId(update.getMessage().getChatId()).isActive()
                && update.getMessage().getText().equals("/start")) {
                return "\n" +
                        "Sənin aktiv olan sorğun var. Sorğuya davam edə və ya \"/stop\" əmrini yazmaqla bu sorğunu dayandıra bilərsən";

        }
        if(update.getMessage().getText().equals("/stop")){
            redisHelperService.updateIsActive(update.getMessage().getChatId(),false);
//            redisHelperService.updateNextQuestion(update.getMessage().getChatId(),"/start");
            return "Aktiv sorğu dayandırıldı! Yeni sorğu yaratmaq üçün \"/start\" yaz və ya \"/start\" yazısına kliklə";
        }
        return null;
    }
}
