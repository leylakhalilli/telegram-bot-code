package com.example.telegrambotcode.sevice;
import com.example.telegrambotcode.message.MessageBot;
import com.example.telegrambotcode.models.Locale;
import com.example.telegrambotcode.repo.LocaleRepo;
import com.example.telegrambotcode.sevice.redis.RedisHelperService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LocaleService {
    @Autowired
    QuestionService questionService;
    @Autowired
    RedisHelperService redisHelperService;
    @Autowired
    ActionService actionService;
    @Autowired
    MessageBot messageBot;
    private final LocaleRepo localeRepo;

    public String getStartQuestion(Update update) {
        Locale locale = null;
        String nextQuestion = null;
        if (update.getMessage().hasText() || update.getMessage().hasContact()) {
            if (messageBot.messageStartAndStop(update) != null) {
                System.out.println("salam");
                return messageBot.messageStartAndStop(update);
            } else {
                redisHelperService.saveRedis(redisHelperService.getByChatId(update.getMessage().getChatId()), update);
                if (answerCheck(update) != null) {
                    return answerCheck(update);
                }
                if (dateFormater(update) != null) {
                    return dateFormater(update);
                }
                locale = getQuestionByKeyAndLang(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()
                        , redisHelperService.getByChatId(update.getMessage().getChatId()).getLang());
                System.out.println(locale);
                questionService.getByQuestionKey(locale.getKey()).getActionList().stream().forEach(item ->
                {
                    if (item.getNextQuestion() != null) {
                        redisHelperService.updateNextQuestion(
                                redisHelperService.getByChatId(update.getMessage().getChatId()), update, item.getNextQuestion());
                    }

                });
            }
        }
        return locale.getValue();


    }

    public List<Locale> getStartButton(Update update) {
        String language = redisHelperService.getByChatId(update.getMessage().getChatId()).getLang();
        String buttonKey = null;
        if (actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                getNextQuestion()) != null &&
                actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                        getNextQuestion()).getActionType().equals("button")) {
            buttonKey = actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                    getNextQuestion()).getButtonName();
            return getButtonByKeyAndLang(buttonKey, language);
        }


        return null;

    }

    public List<Locale> getButtonByKeyAndLang(String key, String language) {
        return localeRepo.getAllByKeyAndLang(key, language);
    }

    public Locale getQuestionByKeyAndLang(String key, String language) {

        return localeRepo.getLocaleByKeyAndLang(key, language);
    }

    public String answerCheck(Update update, SendMessage sendMessage) {
        if (!update.getMessage().getText().equals("/start") && !update.getMessage().getText().equals("/stop") &&
                redisHelperService.getByChatId(update.getMessage().getChatId()) != null
                &&
                actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()) != null
                &&
                actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()).getActionType().equals("button")
        ) {
            String buttonKey = actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                    getNextQuestion()).getButtonName();
            String language = redisHelperService.getByChatId(update.getMessage().getChatId()).getLang();

            for (Locale locale : getButtonByKeyAndLang(buttonKey, language)
            ) {
                if (locale.getValue().equals(update.getMessage().getText())) {
                    if (locale.getKey().equals("lang")) {
                        redisHelperService.updateLanguage(redisHelperService.getByChatId(update.getMessage().getChatId()), update, update.getMessage().getText());
                    }
                    if (update.getMessage().getText().equals("Öz istədiyim yerə")) {
                        redisHelperService.updateButtonType(redisHelperService.getByChatId(update.getMessage().getChatId()), update, "freeText");
                        return "\n" +
                                "Zəhmət olmasa, həmin yerin adını yaz  ✏️";

                    }
                    return null;
                }
            }
            if (redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getButtonType().equals("button")) {
                return "\n" +
                        "Verilən seçimlərdən kənar seçim seçmək olmaz";
            } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English") &&
                    redisHelperService.getByChatId(update.getMessage().getChatId()).getButtonType().equals("button")
            ) {
                return "\n" +
                        "You cannot select an option other than the given options";
            } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getButtonType().equals("button")) {
                return "\n" +
                        "Вы не можете выбрать вариант, отличный от указанных параметров";
            }
            if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English")) {
                return "\n" +
                        "Please give me some time and I will send you the best offers \uD83D\uDE09 If you would like to create another request, you can end this request by typing \"/stop\"";
            } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")) {
                return "\n" +
                        "Mənə bir az vaxt ver, sənin üçün ən yaxşı təklifləri göndərəcəm \uD83D\uDE09 Başqa sorğu yaratmaq istəyirsənsə, \"/stop\" yazmaqla, bu sorğunu bitirə bilərsən.";
            } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")) {
                return "\n" +
                        "Дайте мне немного времени, и я отправлю вам лучшие предложения \uD83D\uDE09 Если вы хотите создать еще один запрос, вы можете завершить этот запрос, набрав «/stop».";
            }
            if (update.getMessage().getText().equals("Öz istədiyim yerə")) {
                return "\n" +
                        "Zəhmət olmasa, həmin yerin adını yaz  ✏️";

            }


        }


        return null;
    }


    public String answerCheck(Update update) {

        if (update.getMessage().getText() != null && !update.getMessage().getText().equals("/start") && !update.getMessage().getText().equals("/stop") &&
                redisHelperService.getByChatId(update.getMessage().getChatId()) != null
                &&
                actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()) != null
                &&
                actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()).getActionType().equals("button")
        ) {
            String buttonKey = actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                    getNextQuestion()).getButtonName();
            String language = redisHelperService.getByChatId(update.getMessage().getChatId()).getLang();

            for (Locale locale : getButtonByKeyAndLang(buttonKey, language)
            ) {
                if (locale.getValue().equals(update.getMessage().getText())) {
                    if (locale.getKey().equals("lang")) {
                        redisHelperService.updateLanguage(redisHelperService.getByChatId(update.getMessage().getChatId()), update, update.getMessage().getText());
                    }
                    if (update.getMessage().getText().equals("Öz istədiyim yerə") || update.getMessage().getText().equals("Yes I know the destination") || update.getMessage().getText().equals("Где я хочу")) {
                        redisHelperService.updateButtonType(redisHelperService.getByChatId(update.getMessage().getChatId()), update, "freeText");
                        if (redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")) {
                            return "\n" +
                                    "Zəhmət olmasa, həmin yerin adını yaz  ✏️";

                        } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English")) {
                            return "\n" +
                                    "Please write the name of the place  ✏️";
                        } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")) {
                            System.out.println(redisHelperService.getByChatId(update.getMessage().getChatId()));
                            return "\n" +
                                    "Пожалуйста, напишите название места  ✏️";

                        }

                    }
                    return null;
                }
            }
            if (update.getMessage().getText() != null && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getButtonType().equals("button")) {
                return "\n" +
                        "Verilən seçimlərdən kənar seçim seçmək olmaz";
            } else if (update.getMessage().getText() != null && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English") &&
                    redisHelperService.getByChatId(update.getMessage().getChatId()).getButtonType().equals("button")
            ) {
                return "\n" +
                        "You cannot select an option other than the given options";
            } else if (update.getMessage().getText() != null && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")
                    && redisHelperService.getByChatId(update.getMessage().getChatId()).getButtonType().equals("button")) {
                return "\n" +
                        "Вы не можете выбрать вариант, отличный от указанных параметров";
            }

        } else {
            if (update.getMessage().getText() != null &&
                    !update.getMessage().getText().equals("/start")
                    && !update.getMessage().getText().equals("/stop") &&
                    redisHelperService.getByChatId(update.getMessage().getChatId()) != null
                    &&
                    actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()) != null
                    &&
                    actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion()).getActionType().equals("freeText")) {
                if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endDate")
                        && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")) {
                    Date startDate = null;
                    try {
                        startDate = new SimpleDateFormat("MM-dd-yyyy").parse(update.getMessage().getText());
                        Date userDate = new Date().from(LocalDate.now().atStartOfDay()
                                .atZone(ZoneId.systemDefault())
                                .toInstant());
                        if (0 < userDate.compareTo(startDate)) {
                            System.out.println(userDate.compareTo(startDate));
                            return "Düzgün tarix daxil edin";
                        }
                    } catch (Exception ex) {
                        return "Düzgün format daxil edin";
                    }

                } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endDate")
                        && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English")) {
                    Date startDate = null;
                    try {
                        startDate = new SimpleDateFormat("MM-dd-yyyy").parse(update.getMessage().getText());
                        Date userDate = new Date().from(LocalDate.now().atStartOfDay()
                                .atZone(ZoneId.systemDefault())
                                .toInstant());
                        if (0 < userDate.compareTo(startDate)) {
                            return "Enter the correct date";
                        }
                    } catch (Exception ex) {
                        return "Enter the correct format";
                    }

                } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endDate")
                        && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")) {
                    Date startDate = null;

                    try {
                        startDate = new SimpleDateFormat("MM-dd-yyyy").parse(update.getMessage().getText());
                        Date userDate = new Date().from(LocalDate.now().atStartOfDay()
                                .atZone(ZoneId.systemDefault())
                                .toInstant());
                        if (0 < userDate.compareTo(startDate)) {
                            return "Введите правильную дату";
                        }
                    } catch (Exception ex) {
                        return "Введите правильный формат";
                    }

                }
            }
        }
        if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("withSomeone")
                && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")) {
            Date endDate = null;
            try {
                endDate = new SimpleDateFormat("MM-dd-yyyy").parse(update.getMessage().getText());
                Date userDate = new Date().from(LocalDate.now().atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                if (0 < userDate.compareTo(endDate)) {
                    return "Düzgün tarix daxil edin";
                }
            } catch (Exception ex) {
                return "Düzgün format daxil edin";
            }

        } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("withSomeone")
                && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English")) {
            Date endDate = null;
            try {
                endDate = new SimpleDateFormat("MM-dd-yyyy").parse(update.getMessage().getText());
                Date userDate = new Date().from(LocalDate.now().atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                if (0 < userDate.compareTo(endDate)) {
                    return "Enter the correct date";
                }
            } catch (Exception ex) {
                return "Enter the correct format";
            }

        } else if (redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("withSomeone")
                && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")) {
            Date endDate = null;

            try {
                endDate = new SimpleDateFormat("MM-dd-yyyy").parse(update.getMessage().getText());
                Date userDate = new Date().from(LocalDate.now().atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                if (0 < userDate.compareTo(endDate)) {
                    return "Введите правильную дату";
                }
            } catch (Exception ex) {
                return "Введите правильный формат";
            }

        } else if (update.getMessage().getText() != null && redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")
                && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("English")) {
            return "\n" +
                    "Please give me some time and I will send you the best offers \uD83D\uDE09 If you would like to create another request, you can end this request by typing \"/stop\"";
        } else if (update.getMessage().getText() != null && redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")
                && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Azərbaycan")) {
            return "\n" +
                    "Mənə bir az vaxt ver, sənin üçün ən yaxşı təklifləri göndərəcəm \uD83D\uDE09 Başqa sorğu yaratmaq istəyirsənsə, \"/stop\" yazmaqla, bu sorğunu bitirə bilərsən.";
        } else if (update.getMessage().getText() != null && redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")
                && redisHelperService.getByChatId(update.getMessage().getChatId()).getLang().equals("Русский")) {
            return "\n" +
                    "Дайте мне немного времени, и я отправлю вам лучшие предложения \uD83D\uDE09 Если вы хотите создать еще один запрос, вы можете завершить этот запрос, набрав «/stop».";
        }

        return null;

    }

    public String dateFormater(Update update) {

        return null;
    }

}
