package com.example.telegrambotcode.sevice;

import com.example.telegrambotcode.config.BotConfig;
import com.example.telegrambotcode.models.User;
import com.example.telegrambotcode.sevice.redis.RedisHelperService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TelegramBot extends TelegramLongPollingBot {
    private final BotConfig botConfig;
    @Autowired
    QuestionService questionService;
    @Autowired
    LocaleService localeService;

    @Autowired
    ActionService actionService;
    @Autowired
    RedisHelperService redisHelperService;
    @Autowired
    UserService userService;

    @Override
    public String getBotUsername() {
        return botConfig.getName();
    }

    @Override
    public String getBotToken() {
        return botConfig.getToken();
    }


    @Override
    public void onUpdateReceived(Update update) {
        SendMessage sendMessage = new SendMessage();
        Long chatId = update.getMessage().getChatId();
        sendMessage.setChatId(chatId);

        getUserDetail(update, sendMessage);

        if (userService.getUserByChatId(update.getMessage().getChatId()) != null) {

            sendMessage.setText(localeService.getStartQuestion(update));

            if (actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                    getNextQuestion()) != null &&
                    actionService.getActionByNextQuestion(redisHelperService.getByChatId(update.getMessage().getChatId()).
                            getNextQuestion()).getActionType().equals("button")) {
                sendMessage.setReplyMarkup(setButton(update));
            } else {
                sendMessage.setReplyMarkup(new ReplyKeyboardRemove(true));
            }
        }
        if (update.getMessage().getText().equals("Öz istədiyim yerə") || update.getMessage().getText().equals("Yes I know the destination") || update.getMessage().getText().equals("Где я хочу")) {
            redisHelperService.updateButtonType(redisHelperService.getByChatId(update.getMessage().getChatId()), update, "freeText");
            sendMessage.setReplyMarkup(new ReplyKeyboardRemove(true));
        }
//        if(redisHelperService.getByChatId(update.getMessage().getChatId()).getNextQuestion().equals("endChat")){
        System.out.println(redisHelperService.getByChatId(update.getMessage().getChatId()));
//        }



        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }



    @SneakyThrows
    public ReplyKeyboard setButton(Update update) {
        SendMessage message = new SendMessage();
        message.setChatId(update.getMessage().getChatId());

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboardList = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        if (localeService.getStartButton(update) == null) {
            return null;

        } else {
            localeService.getStartButton(update).stream().forEach(elem -> {
                KeyboardButton button1 = new KeyboardButton();
                button1.setText(elem.getValue());
                row.add(button1);
            });
        }
        keyboardList.add(row);
        keyboardMarkup.setKeyboard(keyboardList);
        message.setReplyMarkup(keyboardMarkup);
        return message.getReplyMarkup();
    }

    @SneakyThrows
    public ReplyKeyboard setShareContact(Update update) {

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboardList = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        if (userService.getUserByChatId(update.getMessage().getChatId()) == null) {
            KeyboardButton button1 = new KeyboardButton();
            button1.setText("share contact");
            button1.setRequestContact(true);
            row.add(button1);
            keyboardList.add(row);
            replyKeyboardMarkup.setKeyboard(keyboardList);
            return replyKeyboardMarkup;
        }
        return null;
    }

    public void getUserDetail(Update update, SendMessage sendMessage) {
        if(update.getMessage().getText()!=null && userService.getUserByChatId(update.getMessage().getChatId()) == null){
            if (update.getMessage().getText().equals("/stop") || update.getMessage().getText().equals("/start")) {
                sendMessage.setText("tur agentin təklif gondərmesi üçün zəhmət olmasa telefon nömrənizi paylaşardız");
                sendMessage.setReplyMarkup(setShareContact(update));

            }
        }
        else if(update.getMessage().hasContact()){
            userService.saveUser(new User(null, update.getMessage().getChatId(), update.getMessage().getContact().getPhoneNumber(), update.getMessage().getContact().getFirstName() + "" + update.getMessage().getContact().getLastName()));
        }
    }
}

