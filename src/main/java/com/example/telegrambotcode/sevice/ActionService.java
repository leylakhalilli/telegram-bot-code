package com.example.telegrambotcode.sevice;

import com.example.telegrambotcode.models.Action;
import com.example.telegrambotcode.repo.ActionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ActionService {
    public final ActionRepo actionRepo;

    public Action getActionByNextQuestion(String buttonKey) {
        return actionRepo.findByNextQuestion(buttonKey);
    }

}
