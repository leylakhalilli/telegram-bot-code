package com.example.telegrambotcode.sevice.redis;

import com.example.telegrambotcode.models.redis.RedisHelper;
import com.example.telegrambotcode.sevice.QuestionService;
import jakarta.annotation.Resource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Map;

@RequiredArgsConstructor
@Repository
public class RedisHelperService {
    @Autowired
    QuestionService questionService;
    private final String hashReference = "RedisHelper";

    @Resource(name = "template")
    private HashOperations<String, Long, RedisHelper> hashOperations;


    public RedisHelper saveRedis(RedisHelper redis, Update update) {

        if (getByChatId(update.getMessage().getChatId()) != null && update.getMessage().getText() != null && update.getMessage().getText().equals("/start")
                && !getByChatId(update.getMessage().getChatId()).isActive() || update.getMessage().hasContact()) {
            updateIsActive(redis, update, true);
            updateButtonType(getByChatId(update.getMessage().getChatId()), update, "button");
            updateNextQuestion(redis, update, "/start");
        } else if (getByChatId(update.getMessage().getChatId()) == null && update.getMessage().getText() != null && update.getMessage().getText().equals("/start")) {
            hashOperations.putIfAbsent(hashReference, update.getMessage().getChatId(), new RedisHelper(update.getMessage().getChatId(), update.getMessage().getText(), "Azərbaycan", true, "button", null));
        }

        return redis;
    }


    public Map<Long, RedisHelper> getAll() {
        return hashOperations.entries(hashReference);
    }

    public RedisHelper getByChatId(Long id) {
        return hashOperations.get(hashReference, id);
    }


    public void updateLanguage(RedisHelper redis, Update update, String lang) {
        RedisHelper redisHelper = getByChatId(redis.getChatId());
        redisHelper.setLang(lang);
        hashOperations.put(hashReference, update.getMessage().getChatId(), redisHelper);
    }

    public void updateButtonType(RedisHelper redis, Update update, String buttonType) {
        RedisHelper redisHelper = getByChatId(redis.getChatId());
        redisHelper.setButtonType(buttonType);
        hashOperations.put(hashReference, update.getMessage().getChatId(), redisHelper);
    }

    public void updateNextQuestion(RedisHelper redis, Update update, String nextQuestion) {
        RedisHelper helperRedis = getByChatId(redis.getChatId());
        helperRedis.setNextQuestion(nextQuestion);
        hashOperations.put(hashReference, update.getMessage().getChatId(), helperRedis);
    }

    public void updateIsActive(RedisHelper redis, Update update, Boolean isActive) {

        RedisHelper helperRedis = getByChatId(redis.getChatId());
        helperRedis.setActive(isActive);
        hashOperations.put(hashReference, update.getMessage().getChatId(), helperRedis);
    }

    public void deleteByChatId(Long id) {
        questionService.getByQuestionKey(getByChatId(id).getNextQuestion()).getActionList().stream().forEach(item ->
        {
            if (item.getNextQuestion() == null) {
                hashOperations.delete(hashReference, getByChatId(id));
            }
        });
    }


}
