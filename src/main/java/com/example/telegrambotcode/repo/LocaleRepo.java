package com.example.telegrambotcode.repo;


import com.example.telegrambotcode.models.Locale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

public interface LocaleRepo extends JpaRepository<Locale, Long> {

    List<Locale>getAllByKeyAndLang(String key,String lang);
    Locale getLocaleByKeyAndLang(String key,String lang);

}
