package com.example.telegrambotcode.repo.util;

import com.example.telegrambotcode.models.util.RedisHelper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface RedisHelperRepo extends JpaRepository<RedisHelper,Long> {
    RedisHelper getByChatId(Long chatId);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update RedisHelper r set r.nextQuestion=:nextQuestion where r.chatId=:Id")
    void updateNextQuestion(@Param("Id") Long id, @Param("nextQuestion") String nextQuestion);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update RedisHelper r set r.isActive=:isActive where r.chatId=:Id")
    void updateIsActive(@Param("Id") Long id, @Param("isActive") Boolean isActive);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update RedisHelper r set r.lang=:lang where r.chatId=:Id")
    void updateLanguage(@Param("Id") Long id, @Param("lang") String lang);
}
