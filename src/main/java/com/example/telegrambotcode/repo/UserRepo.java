package com.example.telegrambotcode.repo;

import com.example.telegrambotcode.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User,Long> {
    User getByChatId(Long id);

}
