
package com.example.telegrambotcode.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "sessions")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_session")
    @SequenceGenerator(
            name = "seq_session", allocationSize = 1
    )
    private Long Id;
    private Long chatId;

    @OneToMany(mappedBy = "session", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Answer> answer;

    public void setAnswer(List<Answer> answers) {
        for (Answer answer1 : answers) {
            answer1.setSession(this);
        }
        this.answer = answers;

    }
}