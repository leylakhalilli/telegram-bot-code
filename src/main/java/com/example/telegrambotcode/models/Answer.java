package com.example.telegrambotcode.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.lang.reflect.Method;

@Entity
@Table(name = "answer")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Answer  {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_answer")
    @SequenceGenerator(
            name = "seq_answer", allocationSize = 1
    )
    private Long id;

    private String language;
    private String category;
    private String offer;
    private String countryType;
    private String travelType;
    private String destination;
    private String startingPoint;
    private String startDate;
    private String endDate;
    private String budget;
    private String withSomeone;
    @ManyToOne(fetch = FetchType.EAGER)

    @JoinColumn(name = "session_id", referencedColumnName = "id")
    private Session session;


}
