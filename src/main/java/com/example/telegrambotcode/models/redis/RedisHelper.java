package com.example.telegrambotcode.models.redis;

import com.example.telegrambotcode.models.Session;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class RedisHelper implements Serializable {
    @Id
    private Long chatId;
    private String nextQuestion;
    private String lang;
    private boolean isActive;
    private String buttonType;
    private Session session;
}
