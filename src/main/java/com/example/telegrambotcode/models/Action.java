package com.example.telegrambotcode.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="actions")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_action")
    @SequenceGenerator(
            name = "seq_action", allocationSize = 1
    )
    private Long id;
    @ManyToOne
    @JoinColumn(name = "question_id",referencedColumnName = "id")
    private Question question;
    private String actionType;
    private String buttonName;
    private String nextQuestion;
}
